function [Quants, partition] = quantizeSignal (signal)
%quantizes the signal (Amplitude/Phase)
%designed for transmission model of lab1

minSignal = min(signal); %selects min amplitude from signal
maxSignal = max(signal);%max amplitude from signal
%divides the magnitude to 255 values
if minSignal <= 0
    StepSize = (maxSignal + abs(minSignal))/255; 
elseif minSignal <=0 && maxSignal <=0
    StepSize = (maxSignal - minSignal)/255;
else
     StepSize = (maxSignal - minSignal)/255;    
end
partition = minSignal : StepSize : maxSignal - StepSize; %quantization levels (values)
codebook = 0:255; %number of quantization levels
[~, Quants]  = quantiz(signal, partition, codebook); % Quantize
%length(partition) = length(codebook) - 1
%quants is our quantized signal we are workin' with
