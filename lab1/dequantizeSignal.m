function dequantizedSignal = dequantizeSignal (signal, partition)
%this function takes values from partition table and replaces signal values
%of same quantization

%initialize
dequantizedSignal = zeros (1, length(signal));
for index = 1 : length(signal) %length of rxAmplitude and rxPhase are same
    if signal(index) == 0
        dequantizedSignal(index) = 0; %when we use 0 as index it matlab crushes (line 109 use this in full loop)
    else
        dequantizedSignal(index) = partition(signal(index));
    end
end