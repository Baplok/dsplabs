%Sound transmission model
%*made by Igor Shevchenko*
%
%Quention1. When we filter the signal to 24KHz it doesnt matter. But male and female voices use different frequencies.
%For the human ear male voice sound required to have better quality than
%female.
%Question2. for each purpose we have different BER we can allow. The aim is
%to understand what information was transmitted. So alowed SNR depends on the quality of received signal we
%want to have. For each modulation type we have different BER curves. For
%QPSK BER of 1e-6 it's between 10 and 11
%Question3. We have 3 reasons why the Fourier spectra are different.
%They are: filtering the signal (lowpass 24KHz), quantization and linear interpolation (upsampling the signal)

clc
clear
%% load sound file
[FileName, PathName] = uigetfile ('*.flac','Select the MATLAB code file'); %select file
[signal, Fs] = audioread ([PathName FileName]); %get signal and sample freq
%% play sound (if necessary - uncomment it)
%sound(signal, Fs)
%% initialize filter
lowpassFilter = designfilt ('lowpassfir', 'PassbandFrequency', 0.45,...
             'StopbandFrequency', 0.5, 'PassbandRipple', 0.5, ...
             'StopbandAttenuation', 65, 'DesignMethod', 'kaiserwin');
         
%% apply filter (24Hz resampling)
filteredSignal = filter (lowpassFilter, signal);

%% resampling signal
downsampledSignal = downsample (filteredSignal, 2); %throws out every second value

%% prepare for quantization - divide amplitude and phaze values
 signalAmplitude = downsampledSignal(:, 1);
 signalPhase = downsampledSignal (:, 2);
 
%% quantize Amplitude and Phase (user function used)
%the aim is to divide the signal(magnitude) to 256 = 2e8 intervals (8bit)
[amplitudeQuants, partitionAmplitude] = quantizeSignal(signalAmplitude); %look inside quantizeSignal.m for info
[phaseQuants, partitionPhase] = quantizeSignal(signalPhase);

%% use Amplitude and Phase quants to construct a message (column-vector)
%place amplitudeQuant(aQ) and phaseQuant(pQ) sequently: aQ-pQ-aQ-pQ-...
%initialization
Tx = zeros (1, length(signal)); %signal to transmit
for position = 1 : length(phaseQuants)
            Tx (2*position-1) = amplitudeQuants(position); %amplitudes positions are odd: 1,3,5,...
            Tx (2*position) = phaseQuants(position);%phase positions are even: 2,4,6,...
end

%% make Tx binary
binaryTx = de2bi(Tx);
binReshapedTx = reshape(binaryTx', 1, []);%put our 8-bit signal messages to one column: length(binaryTx)*8
binReshapedTx = binReshapedTx';

%% Simulate transmission (Modulation--Noise--Demodulation) + count errors (use ERC)
%this step requires 4 objects (for each step) to be generated
hMod = comm.QPSKModulator('PhaseOffset', pi/4); %QPSK-modulator
hAWGN = comm.AWGNChannel('NoiseMethod',...
    'Signal to noise ratio (SNR)','SNR',11);%WhiteGaussianNoise
hDemod = comm.QPSKDemodulator('PhaseOffset',pi/4);%QPSK-demodulator
hError = comm.ErrorRate;%ERC block

%------------- use objects --------------------
%we use step to use objects like a functions
%*search for 'System Object Methods' in matlab help for more info*
        modSignal = step(hMod, binReshapedTx);
        noisySignal = step(hAWGN, modSignal);
        Rx = step(hDemod, noisySignal); %received data
        errorStats = step(hError, binReshapedTx, Rx);
%-----------------------------------------------
% print information about error rate and number of errors
fprintf('Error rate = %f\nNumber of errors = %d\n', ...
    errorStats(1), errorStats(2))
  
%% reconstructing of received message (Rx)
%at first - make errors binaty -set to 0
for i = 1 : length(Rx)
    if Rx(i) >= 1
        Rx(i) = 1;
    end
end
%% turn binary data to decimal
reshapedRx = reshape(Rx, 8, length(Rx)/8)';
decimalRx = bi2de(reshapedRx);

%% separate Amplitude and Phase
%initialize
rxAmplitude = zeros (1, length(decimalRx)/2);
rxPhase = zeros (1, length(decimalRx)/2);
%fill
 for index = 1 : length(decimalRx)/2
    rxAmplitude(index) = decimalRx(2*index-1);
    rxPhase(index) = decimalRx(2*index);
 end

%% dequantization of Amplitude and Phase (user function used)
%make out rxAmplitude and rxPhase values to be restored using partition
%table to have real values instead of quantization values
%look inside dequantizeSignal.m for more info
recoveredAmplitude = dequantizeSignal (rxAmplitude, partitionAmplitude);
recoveredPhase = dequantizeSignal (rxPhase, partitionPhase);

%% resample the signal (from 24KHz to 48KHz)
upsampledAmplitude = resample (recoveredAmplitude, 2, 1);
upsampledPhase = resample (recoveredPhase, 2 ,1);

%% make Amplitude and Phase work together
soundRx = zeros(length(upsampledAmplitude), 2);
soundRx(:, 1) = upsampledAmplitude;
soundRx(:, 2) = upsampledAmplitude; 

%% play sound (if necessary - uncomment it)
% sound(soundRx, Fs)

%% DFT figures
%spectrum of signal
subplot (2,1,1)
plot(real(fft(signal)));
title('signal');

%spectrum of transmitted signal
subplot (2,1,2)
plot(real(fft(soundRx)));
title('transmitted signal');

  
 