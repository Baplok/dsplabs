function varargout = shGUI(varargin)

%% ABOUT
%This script creates a GUI based on the figure with the same name.

%% Initialization
% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @shGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @shGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before shGUI is made visible.
function shGUI_OpeningFcn(hObject, eventdata, handles, varargin)
%% Open-function + defining tabs
% Set the colors indicating a selected/unselected tab
handles.unselectedTabColor = get(handles.tab1text,'BackgroundColor');
handles.selectedTabColor = handles.unselectedTabColor-0.1;

% Set units to normalize for easier handling
set(handles.tab1text,'Units','normalized')
set(handles.tab2text,'Units','normalized')
set(handles.tab1Panel,'Units','normalized')
set(handles.tab2Panel,'Units','normalized')

% Create tab labels (as many as you want according to following code template)

% Tab 1
pos1=get(handles.tab1text,'Position');
handles.a1=axes('Units','normalized',...
                'Box','on',...
                'XTick',[],...
                'YTick',[],...
                'Color',handles.selectedTabColor,...
                'Position',[pos1(1) pos1(2) pos1(3) pos1(4)+0.01],...
                'ButtonDownFcn','simpletab(''a1bd'',gcbo,[],guidata(gcbo))');
handles.t1=text('String','Setup Tab',...
                'Units','normalized',...
                'Position',[(pos1(3)-pos1(1))/2,pos1(2)/2+pos1(4)],...
                'HorizontalAlignment','left',...
                'VerticalAlignment','middle',...
                'Margin',0.001,...
                'FontSize',8,...
                'Backgroundcolor',handles.selectedTabColor,...
                'ButtonDownFcn','simpletab(''t1bd'',gcbo,[],guidata(gcbo))');

% Tab 2
pos2=get(handles.tab2text,'Position');
pos2(1)=pos1(1)+pos1(3);
handles.a2=axes('Units','normalized',...
                'Box','on',...
                'XTick',[],...
                'YTick',[],...
                'Color',handles.unselectedTabColor,...
                'Position',[pos2(1) pos2(2) pos2(3) pos2(4)+0.01],...
                'ButtonDownFcn','simpletab(''a2bd'',gcbo,[],guidata(gcbo))');
handles.t2=text('String','Results Tab',...
                'Units','normalized',...
                'Position',[pos2(3)/2,pos2(2)/2+pos2(4)],...
                'HorizontalAlignment','left',...
                'VerticalAlignment','middle',...
                'Margin',0.001,...
                'FontSize',8,...
                'Backgroundcolor',handles.unselectedTabColor,...
                'ButtonDownFcn','simpletab(''t2bd'',gcbo,[],guidata(gcbo))');
           
            
% Managing panels (placing them in the correct position and managing visibilities)
pan1pos=get(handles.tab1Panel,'Position');
set(handles.tab2Panel,'Position',pan1pos)
set(handles.tab2Panel,'Visible','off')


% Update handles structure
guidata(hObject, handles);

% UIWAIT makes shGUI wait for user response (see UIRESUME)
% uiwait(handles.simpletabfig);


% --- Outputs from this function are returned to the command line.
function varargout = shGUI_OutputFcn(hObject, eventdata, handles) 

%%       FUNCTIONS BELOW ARE KIND OF OBJECTS FUNCTIONS
%                               |
%                               |
%                               |
%                               |
%                               V

% Text object 1 callback (tab 1)
function t1bd(hObject,eventdata,handles)

set(hObject,'BackgroundColor',handles.selectedTabColor)
set(handles.t2,'BackgroundColor',handles.unselectedTabColor)
set(handles.a1,'Color',handles.selectedTabColor)
set(handles.a2,'Color',handles.unselectedTabColor)
set(handles.tab1Panel,'Visible','on')
set(handles.tab2Panel,'Visible','off')


% Text object 2 callback (tab 2)
function t2bd(hObject,eventdata,handles)

set(hObject,'BackgroundColor',handles.selectedTabColor)
set(handles.t1,'BackgroundColor',handles.unselectedTabColor)
set(handles.a2,'Color',handles.selectedTabColor)
set(handles.a1,'Color',handles.unselectedTabColor)
set(handles.tab2Panel,'Visible','on')
set(handles.tab1Panel,'Visible','off')


% Axes object 1 callback (tab 1)
function a1bd(hObject,eventdata,handles)

set(hObject,'Color',handles.selectedTabColor)
set(handles.a2,'Color',handles.unselectedTabColor)

set(handles.t1,'BackgroundColor',handles.selectedTabColor)
set(handles.t2,'BackgroundColor',handles.unselectedTabColor)

set(handles.tab1Panel,'Visible','on')
set(handles.tab2Panel,'Visible','off')



% Axes object 2 callback (tab 2)
function a2bd(hObject,eventdata,handles)

set(hObject,'Color',handles.selectedTabColor)
set(handles.a1,'Color',handles.unselectedTabColor)
set(handles.t2,'BackgroundColor',handles.selectedTabColor)
set(handles.t1,'BackgroundColor',handles.unselectedTabColor)
set(handles.tab2Panel,'Visible','on')
set(handles.tab1Panel,'Visible','off')


function distanceEdit_Callback(hObject, eventdata, handles)
% hObject    handle to distanceEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of distanceEdit as text
%        str2double(get(hObject,'String')) returns contents of distanceEdit as a double


% --- Executes during object creation, after setting all properties.
function distanceEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to distanceEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function snrEdit_Callback(hObject, eventdata, handles)
% hObject    handle to snrEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of snrEdit as text
%        str2double(get(hObject,'String')) returns contents of snrEdit as a double


% --- Executes during object creation, after setting all properties.
function snrEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to snrEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in wtypePop.
function wtypePop_Callback(hObject, eventdata, handles)
% hObject    handle to wtypePop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns wtypePop contents as cell array
%        contents{get(hObject,'Value')} returns selected item from wtypePop


% --- Executes during object creation, after setting all properties.
function wtypePop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to wtypePop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in chtypePop.
function chtypePop_Callback(hObject, eventdata, handles)
% hObject    handle to chtypePop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns chtypePop contents as cell array
%        contents{get(hObject,'Value')} returns selected item from chtypePop


% --- Executes during object creation, after setting all properties.
function chtypePop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to chtypePop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in launchButton.
function launchButton_Callback(hObject, eventdata, handles)
%% HERE WE GO TO ADD A CALLBACK TO OUR RED BUTTON
% hObject    handle to launchButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Initialization (read parameters from GUI)
Params.realDistance = str2num(get(handles.distanceEdit,'String'));
Params.snr = str2num(get(handles.snrEdit,'String'));
Params.waveform = get(handles.wtypePop,'Value');
Params.channel = get(handles.chtypePop,'Value');
Params.object = get(handles.objectPop,'Value');

%% Create signal
%some constant values:
C = 3e8; %speed of light
Signal.sampleFrequency = 8e9; %Sample rate
Signal.carierFrequency = 1e9; %Carrier frequency
timeline = 0 : 1/Signal.sampleFrequency : 1e-4;
Signal.wavelength = C / Signal.carierFrequency; % calc wavelength
Signal.attenuation = pow2(4*pi*2*Params.realDistance/100*Signal.wavelength); %path loss

%due to text in 'String' rows (number of row), return the type of signal
if Params.waveform == 1 %sinusoid
    Signal.wave = sin(2*pi*Signal.carierFrequency*timeline);
elseif Params.waveform == 2 %sawtooth
    Signal.wave = sawtooth(2*pi*Signal.carierFrequency*timeline);
elseif Params.waveform == 3 %square
    Signal.wave = square(2*pi*Signal.carierFrequency*timeline);
end

%now we have a signal to send. let's set TX axis in the GUI according to that.
axes(handles.txAxes);
cla;
plot(timeline, Signal.wave);
set(handles.txAxes, 'Xlim', [0 10/Signal.carierFrequency], 'Ylim', [-1.1 1.1]);

%% 'radar --> object' path distortion
if Params.channel == 2 %AWGN
    Signal.receivedByObject = awgn(Signal.wave, Params.snr)/Signal.attenuation;
elseif Params.channel == 3 %rayleigh
    objectSpeed = randi([10 100],1,1); % random speed of our object
    Signal.dopplerFrequency = objectSpeed / Signal.wavelength; %doppler frequency
    channelFilter = rayleighchan(1/Signal.sampleFrequency, Signal.dopplerFrequency); %creating the rayleigh channel
    Signal.receivedByObject = real(filter(channelFilter, Signal.wave)); %add fading using rayleigh channel object
    Signal.receivedByObject = awgn(Signal.receivedByObject, Params.snr)/Signal.attenuation; % + noise
else %only path losses (if None selected)
    Signal.receivedByObject = Signal.wave/Signal.attenuation;
end

%% Add reflection using Radar Target system object
%
% https://www.mathworks.com/help/phased/ug/radar-target.html
%
if Params.object == 2 %small plane
    RCS = 0.001; %rcs for small object
    %create a target with props:
    objectProperties = phased.RadarTarget('MeanRCS', RCS, ...
        'PropagationSpeed', C, ...
        'OperatingFrequency', Signal.carierFrequency);
    Signal.reflected = step(objectProperties, Signal.receivedByObject);
    
elseif Params.object == 3 %big plane
    RCS = 0.01; % rcs for big object
    %create a target with props:
    objectProperties = phased.RadarTarget('MeanRCS', RCS, ...
        'PropagationSpeed', C, ...
        'OperatingFrequency', Signal.carierFrequency);
    Signal.reflected = step(objectProperties, Signal.receivedByObject);
    
else %perfect mirror
    Signal.reflected = Signal.receivedByObject;
end
    
%% 'object --> radar' path distortion
if Params.channel == 2 %AWGN
    Signal.receivedByRadar = awgn(Signal.reflected, Params.snr)/Signal.attenuation;
elseif Params.channel == 3 %rayleigh
    Signal.receivedByRadar = real(filter(channelFilter, Signal.reflected)); %add fading using rayleigh channel object
    Signal.receivedByRadar = awgn(Signal.receivedByRadar, Params.snr)/Signal.attenuation; % + noise
else %only path losses (if None selected)
    Signal.receivedByRadar = Signal.reflected/Signal.attenuation;
end

%% receive delayed and distorted signal, plot it
%received signal will have a time shift corresponding to the distance:
Signal.delay = 2*Params.realDistance/C; %not quite fair but usable...
Signal.delaySamples = ceil(Signal.delay * Signal.sampleFrequency); %count samples and round
Signal.received = zeros(1, length(Signal.wave) + Signal.delaySamples);
Signal.received(Signal.delaySamples+1:end) = Signal.receivedByRadar;

%now we have a received signal. let's set RX axis in the GUI according to that.
timeline = 0 : 1/Signal.sampleFrequency : (length(Signal.received)-1)/Signal.sampleFrequency;
axes(handles.rxAxes);
cla;
plot(timeline, Signal.received);
set(handles.rxAxes, 'Xlim', [0 100/Signal.carierFrequency]);

%% find max of received signal ACF -> calc the distance
%initialization of CF:
Signal.corr = zeros(1, 2*length(Signal.received)-1);
Signal.lags = zeros(1, 2*length(Signal.received)-1);

Signal.scaledWave = zeros(1,length(Signal.received));
Signal.scaledWave(1:length(Signal.wave)) = Signal.wave;

[Signal.corr, Signal.lags] = xcorr(Signal.received, Signal.scaledWave,'coef'); %cross correlation between transmitted and received
Signal.maxCorrPos = max(find(Signal.corr == max(Signal.corr)));

%for positive lags
axes(handles.corrAxes);   %Chose axes for plotting received signal
cla;
stem(Signal.lags(length(Signal.lags)/2:end),Signal.corr(length(Signal.corr)/2:end))

%calc distance
Signal.calculatedSamples = abs(Signal.maxCorrPos-length(Signal.scaledWave));
Signal.calculatedDelay = Signal.calculatedSamples / Signal.sampleFrequency;
Signal.calculatedDistance = Signal.calculatedDelay * C /2;
set(handles.calcDistanceText,'String',['calculated distance: ',num2str(Signal.calculatedDistance)]);

% --- Executes on selection change in objectPop.
function objectPop_Callback(hObject, eventdata, handles)
% hObject    handle to objectPop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns objectPop contents as cell array
%        contents{get(hObject,'Value')} returns selected item from objectPop


% --- Executes during object creation, after setting all properties.
function objectPop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to objectPop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
