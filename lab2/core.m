%*igorshevchenko
%sound noise reduction based on filtering and spectrum analysis
%
%               *readme*
%some variables were saved as structure fields
%to get audio signal properties type 'Original' in command window
%for more information visit
%'https://www.mathworks.com/help/matlab/structures.html'
%
%             *** NOTE ***
%it's hardly recommended to comment out all sections except one your are
%looking at, in the other case, too many figures will immediately appear.
%
%
%
%                                 file was tested on 'MATLAB R2016b'

clc
clear

% %_________________________________________________________________________
% 
% %% 1. Filter a distorted signal in 1D
% %% 1.1 initialization
% [Original.signal, Original.sampleFrequency] = audioread ('fragment14.wav'); %load the signal
% %sound (Original.signal, Original.sampleFrequency) %play the original sound file
% %% 1.2 original signal analysis
% Original.length = length(Original.signal);%get length of the signal
% Original.fourier = fft(Original.signal); %get the DFT vector (signalFourier)
% Original.power = ((Original.fourier).* conj(Original.fourier) / Original.length); %get Y-axis
% frequencyValues = Original.sampleFrequency * (1 : (Original.length/2)) / Original.length; %get X-axis values for DFT plot
% 
% %plotting DFT of the original audio signal
% figure(1);
% subplot (3,1,1);
% plot (frequencyValues, Original.power(1 : Original.length/2));
% title ('Original Signal Spectrum');
% xlabel ('normalized frequency');
% ylabel ('power');
% axis([0 2500 0 2.6]);
% 
% %to see our signal from another point of view we could also get its
% %spectrogram:
% figure(2);
% spectrogram (Original.signal);
% 
% %% 1.3 getting proper filter and signal filtering
% bandpassFilter = createbpf; %get pre-designed filter from function file
% %plot main filter properties
% freqz (bandpassFilter); %frequency response
% phasez (bandpassFilter); %phase response
% phasedelay (bandpassFilter);
% grpdelay (bandpassFilter);%group phase delay
% %due to there characteristics we could see what type of filer we use
% %(FIR/IIR) and predict how it will affect our signal (e.g. phase delay=0 =>
% %FIR; impulse response -> is it rough or not, etc.)
% 
% %filter our signal
% Filtered.signal = filter (bandpassFilter.Numerator, 1, Original.signal);
% %% 1.4 filtered signal analysis
% Filtered.fourier = fft(Filtered.signal); %get the DFT vector (signalFourier)
% Filtered.power = (Filtered.fourier).* conj(Filtered.fourier) / Original.length;
% %sound (Filtered.signal, Original.sampleFrequency) %play filtered sound file
% 
% %plotting DFT
% figure(1);
% subplot (3,1,3);
% plot (frequencyValues, Filtered.power(1 : Original.length/2));
% title ('Filtered Signal Spectrum');
% xlabel ('normalized frequency');
% ylabel ('power');
% axis([0 2500 0 2.6]);
% 
% %% 1.5 Displaying the autocorrelation of the removed noise with 95% confidence intervals
% rng default
% difference = abs(Original.fourier - Filtered.fourier); %get difference between signal and filtered one
% differenceLength = length (difference); %take length of difference
% [crossCorrelation, lags] = xcorr (difference, 20, 'coeff'); %autocorrelation of "difference"
% 
% %create lower and upper 95% confidence bounds
% criticalValue = sqrt(2) * erfinv(0.95); %critical value
% lowConf = - criticalValue / sqrt(differenceLength); %lower bound
% upConf = criticalValue / sqrt(differenceLength); %upper bound
% 
% %plot the sample autocorrelation
% figure(7);
% stem(lags, crossCorrelation, 'filled')
% hold on
% plot (lags, [lowConf;upConf] * ones(size(lags)), 'r');
% hold off
% ylim([lowConf-0.03 1.05]);
% %% 1.6 making new '.wav' file with filtered signal
% %write filtered sound as a new .wav file
% audiowrite('filteredFragment14.wav', Filtered.signal, Original.sampleFrequency);
% 
% %% 1.7 conclusion
% % by method described in 1.1 - 1.4 we can filter noise if it has different
% % frequencies than voice frequency domain
% % changing bandpassfilter we can cut off useless frequencies
% 
% %as we can see we have experienced some lack of success in operations above
% %using third-party software 'Audacity' I've cut noise from signal and
% %uploaded noise sound to analyze its spectrum.
% [Noise.signal, Noise.sampleFrequency] = audioread ('fragment14_noise.wav');
% %sound (Noise.signal, Noise.sampleFrequency) %play noise sound file
% Noise.length = length(Noise.signal);
% Noise.fourier = fft(Noise.signal); %get the DFT vector (signalFourier)
% Noise.power = (Noise.fourier).* conj(Noise.fourier) / Noise.length;
% noiseFrequencyValues = Noise.sampleFrequency * (1 : (Noise.length/2)) / Noise.length;
% 
% %adds our plot to figure 1 window to compare signals
% figure(1);
% subplot (3,1,2);
% plot (noiseFrequencyValues, Noise.power(1 : Noise.length/2));
% title ('Noise Signal Spectrum');
% xlabel ('normalized frequency');
% ylabel ('power');
% axis([0 2500 0 0.1]);
% 
% %it seems like noise is spread all over the signal so it's impossible to
% %filter it using bandpass filters
% 
% %__________________________________________________________________________
% 
% %% 2. Frequency thresholding in 2D
% %% 2.1 loading image and viewing it
% Image = load(fullfile(matlabroot,'examples','wavelet', 'jump.mat')); %load image
% figure('Name', 'Original Image');
% imshow(Image.jump, []); %view image
% %% 2.2 Showing the Fourier spectrum of the signal in 2-D
% Image.fourier = fft2 (Image.jump);
% Image.fourier = fftshift(Image.fourier); % Center FFT
% 
% Image.magnitude = abs(Image.fourier); % Get the magnitude
% Image.magnitude = log(Image.magnitude + 1); % Use log, for perceptual scaling, and +1 since log(0) is undefined
% Image.magnitude = mat2gray(Image.magnitude); % Use mat2gray to scale the image between 0 and 1
% 
% figure('Name', '2-D signal spectrum');
% imshow(Image.magnitude, []); % Display the spectrum of the signal in 2-D
% %% 2.3 Setting an appropriate hard or soft threshold
% 
% Image.hardThreshold = zeros(986, 1577);%hard window initialization
% Image.hardThreshold (250:750, 550:1050) = 1; %hard window bounds setup
% %note that the smaller are bounds,  than softer image will be as filtered result
% Image.softThreshold = round(Image.magnitude);%use round to make a soft threshold window
% %imshow(Image.hardThreshold, []); % display hard ts
% %imshow(Image.softThreshold,[]); % display soft ts
% %% 2.4 Showing the filtered Fourier spectrum of the signal
% %let's show how threshold affects spectrum
% Image.hardFilteredMagnitude = Image.magnitude .* Image.hardThreshold;
% Image.softFilteredMagnitude = Image.magnitude .* Image.softThreshold;
% 
% figure('Name', 'Hard Filtered Fourier Spectrum');
% imshow(Image.hardFilteredMagnitude, []);
% figure('Name', 'Soft Filtered Fourier Spectrum');
% imshow(Image.softFilteredMagnitude, []);
% %% 2.5 Showing the reconstructed image and the residuals
% %finally, apply our filter to fourier and take inverse fft to view result
% Image.hardFiltered = Image.fourier .* Image.hardThreshold;
% Image.hardFiltered = ifft2(ifftshift(Image.hardFiltered));% take inverse fourier and shift back
% 
% Image.softFiltered = Image.fourier .* Image.softThreshold;
% Image.softFiltered = ifft2(ifftshift(Image.softFiltered));
% 
% figure('Name', 'Hard Filtered');
% imshow(Image.hardFiltered, []);
% figure('Name', 'Soft Filtered');
% imshow(Image.softFiltered, []);
% %as wee could see soft filtering brings very blutty image, to see
% %image detailse, some noise is normal. we can adjust noise removal by
% %resizing threshold bounds
% 
% %_____________________________________________________________________________
% 
% %% 3. Wavelet transformation
% %load image again
% Image = load(fullfile(matlabroot,'examples','wavelet', 'jump.mat')); %load image
% figure('Name', 'Original Image');
% imshow(Image.jump, []); %view image
% noisedImage = Image.jump; %make variable to work with
% 
% %using wavelet GUI we could try different wavelets (types and levels) and
% %compare de-noising results
% 
% %at first I user 'haar' level-2 with automatically generated "scaled AWGN"
% %threshold
% %resulting de-noising progress was saved to 'waveletdenoise.m' file
% denoisedImage = waveletdenoise(noisedImage); %call function that makes de-noising
% figure('Name', 'Wavelet haar level-2 filtered image');
% imshow(denoisedImage, []);
% %we get rid of some noise but it's still too sharp.
% %trying to increase wavelet-level and different types will help to reach
% %good results
% 
% %lets see how 'bior3.5' 5-level wavelet will work
% waveletName = 'bior3.5';
% waveletLevel = 5;
% [decompositionVector, bookkepingMatrix] = wavedec2(noisedImage, waveletLevel, waveletName); % returns the wavelet decomposition 
% %of our 'noisedImage' at level 'waveletLevel' using the wavelet named in character vector
% thresholdValues = wthrmngr('dw2ddenoLVL', 'penalhi', decompositionVector, bookkepingMatrix, 3); %returns threshold due to options
% %more information at https://www.mathworks.com/help/wavelet/ref/wthrmngr.html
% SORH = 's'; %Soft Or Hard thresholding
% [XDEN,cfsDEN,dimCFS] = wdencmp('lvd', decompositionVector, bookkepingMatrix, waveletName, waveletLevel, thresholdValues, SORH); %denoising process function
% figure('Name', 'Wavelet bior3.5 level-5 filtered image');
% imshow(XDEN, []);
% %we cound see that bior3.5 gives much more accurate view without noise but
% %still sharp enough
% 
% %in GUI:
% % *window most of the noise is spead across low levels of wavelet 
% %decomposition, to reduce it bigger values have to be set in GUI
% % *reconstruction of image was made automatically due to profile settings set
% %to de-noising
% 
% %____________________________________________________________________________

%% 4. Singular Value Decomposition
Image = load(fullfile(matlabroot,'examples','wavelet', 'jump.mat')); %load image
noisedImage = im2double(Image.jump); %convert value of image to double to work with svd
[U,S,V]=svd(noisedImage); %decomposing and image
figure('Name', 'Noised Image');
imshow(noisedImage,[]); %show noised image
singVal=diag(S);%set singular values variable

%show all singular values
figure('Name', 'Singular Values');
semilogy(singVal(1:length(singVal)),'-ro','LineWidth',1);
legend('singular values');
xlabel('singular values');
grid on;

%lets have a loot at first 10 singular values
for i = 1 : 10
clearMatrix = zeros(986,1577); %make 0 matrix to delete some singular values
clearMatrix (i,i) = 1; %set some singular values to 1

editedS = S.*clearMatrix; %edit original singular values 'S' matrix

reconstructedImage = U*editedS*(V'); %reconstruction of image = U*S*V'

figure;
imshow(reconstructedImage, []); %show results
end
%the larger indexes of singular matrix we see, then more detailes they add
%to image
%I've chosen 300 indexes to be kept
clearMatrix = zeros(986,1577); %make 0 matrix one more time
clearMatrix (1:300, 1:1+300) = 1; %set some singular values from 1 to 300
editedS = S.*clearMatrix; %edit original singular values 'S' matrix
reconstructedImage = U*editedS*(V'); %reconstruction of image = U*S*V'

figure('Name', 'Result Image');
imshow(reconstructedImage, []); %show results
%our image is less contrast

%___________________________________________________________________________

% %% 5. HCA on satellite images
% % We use a hyperspectral data set with 8 detection windows of the same area. 
% imageOriginal = im2double(imread('RomanColosseum_WV2mulitband.tif')); %load image
% imageReshaped = reshape(imageOriginal, [419*8,658]); %reshapind 3-D to 2-D
% imageLinked = linkage (imageReshaped); %makes clusters according to distances between values
% figure
% dendrogram(imageLinked) %to see clusters we received
% %we coul take clusters that are wider to the most significant ones or most
% %popular
% %for some rare-effects on image we should take a look at more thin clusters

