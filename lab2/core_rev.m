%*igorshevchenko
%rev1
%
%               *readme*
%some of the variables were saved as the structure fields
%to get the audio signal properties type 'Original' in the command window
%for more information visit
%'https://www.mathworks.com/help/matlab/structures.html'
%
%             *** NOTE ***
%it's hardly recommended to run sections separately
%
%
%                                 file was tested on 'MATLAB R2016b'
%                                 8.11.2016

clc
clear

%_________________________________________________________________________

%% 1. Filter a distorted signal in 1D
%% 1.1 initialization
%clear variables that are not used further
clearvars
%_________________________________________

[Original.signal, Original.sampleFrequency] = audioread ('fragment14.wav'); %load signal
%sound (Original.signal, Original.sampleFrequency) %play original sound file
%% 1.2 original signal analysis
Original.length = length(Original.signal);%get length of the signal
Original.fourier = fft(Original.signal); %get the DFT vector (signalFourier)
Original.power = ((Original.fourier).* conj(Original.fourier) / Original.length); %get Y-axis
frequencyValues = Original.sampleFrequency * (1 : (Original.length/2)) / Original.length; %get X-axis values for DFT plot

%plotting DFT of original audio signal
figure(1);
subplot (3,1,1);
plot (frequencyValues, Original.power(1 : Original.length/2));
title ('Original Signal Spectrum');
xlabel ('normalized frequency');
ylabel ('power');
axis([0 2500 0 2.6]);

%to see our signal from another point of view we could also get its
%spectrogram:
figure(2);
spectrogram (Original.signal);
%this spectrogram shows us that the most significant values are spread from
%0.02*x to 0.47*x, where x = pi * Original.sampleFrequency
%the most powerful signal is concentrated in the area near 0.1*x
%
%using a lowpass will help if the noise is spread all over the spectrum, in
%the other case we should try a bandpass and catch the significant signal
%from the noised data

%% 1.3 getting proper filter and signal filtering
bandpassFilter = createbpf; %get the pre-designed filter from function file
%plot main filter properties
freqz (bandpassFilter); %frequency response
phasez (bandpassFilter); %phase response
phasedelay (bandpassFilter);
grpdelay (bandpassFilter);%group phase delay
%due to there characteristics we could see what type of filer we use
%(FIR/IIR) and predict how it will affect our signal (e.g. phase delay=0 =>
%FIR; impulse response -> is it rough or not, etc.)

%filter our signal
Filtered.signal = filter (bandpassFilter.Numerator, 1, Original.signal);
%% 1.4 filtered signal analysis
%clear variables that are not used further
clearvars bandpassFilter
%_________________________________________

Filtered.fourier = fft(Filtered.signal); %get the DFT vector (signalFourier)
Filtered.power = (Filtered.fourier).* conj(Filtered.fourier) / Original.length;
%sound (Filtered.signal, Original.sampleFrequency) %play filtered sound file

%plotting DFT
figure(1);
subplot (3,1,3);
plot (frequencyValues, Filtered.power(1 : Original.length/2));
title ('Filtered Signal Spectrum');
xlabel ('normalized frequency');
ylabel ('power');
axis([0 2500 0 2.6]);

%% 1.5 Displaying the autocorrelation of the removed noise with 95% confidence intervals
%clear variables that are not used further
clearvars frequencyValues 
%_________________________________________

rng default %default controlls for random number generator
Difference.value = abs(Original.signal - Filtered.signal); %get difference between signal and filtered one
Difference.length = length (Difference.value); %take length of difference

[Difference.crossCorrelation, Difference.lags] = xcorr (Difference.value, 20, 'coeff'); %autocorrelation of "difference"
%create lower and upper 95% confidence bounds
Difference.criticalValue = sqrt(2) * erfinv(0.95); %critical value
Difference.lowConf = - Difference.criticalValue / sqrt(Difference.length); %lower bound
Difference.upConf = Difference.criticalValue / sqrt(Difference.length); %upper bound

%plot the sample autocorrelation
figure(7);
stem(Difference.lags, Difference.crossCorrelation, 'filled') %plots vetrical lines in the positions that are marked by lags
hold on
plot (Difference.lags, [Difference.lowConf; Difference.upConf] * ones(size(Difference.lags)), 'r');
hold off
ylim([-0.03 1.05]);
%% 1.6 making new '.wav' file with filtered signal
%clear variables that are not used further
clearvars Difference 
%_________________________________________

%write filtered sound as a new .wav file
[fileName, pathName] = uiputfile('.wav','Save sound','new_sound.wav'); %lets return filename and pathname that user specifies
if isequal(fileName,0) || isequal(pathName,0) %if file or path not indentified, we cancel saving
   return
else
    savingProps = fullfile (pathName, fileName); %chain path and name to one char - file
    %fullfile makes something like '.../home/user/documents/sound.wav'
    audiowrite (savingProps, Filtered.signal, Original.sampleFrequency); %writes the signal to the specified path/name from savingProps
end

%% 1.7 conclusion
%clear variables that are not used further
clearvars 
%_________________________________________

% by method described in sections 1.1 - 1.4 we can filter noise if it has different
% frequencies than a voice frequency domain
% changing the bandpassfilter we can cut off useless frequencies

%As we can see we have experienced some lack of success in the operations
%above.
%Using the third-party software 'Audacity' I've cut noise from the signal and
%uploaded this noise sound to analyze its spectrum.
[Noise.signal, Noise.sampleFrequency] = audioread ('fragment14_noise.wav');
%sound (Noise.signal, Noise.sampleFrequency) %play the noise sound file
Noise.length = length(Noise.signal);
Noise.fourier = fft(Noise.signal); %get the DFT vector (signalFourier)
Noise.power = (Noise.fourier).* conj(Noise.fourier) / Noise.length;
noiseFrequencyValues = Noise.sampleFrequency * (1 : (Noise.length/2)) / Noise.length;

%adds our plot to the figure 1 window to compare signals
figure(1);
subplot (3,1,2);
plot (noiseFrequencyValues, Noise.power(1 : Noise.length/2));
title ('Noise Signal Spectrum');
xlabel ('normalized frequency');
ylabel ('power');
axis([0 2500 0 0.1]);

%it seems like noise is spread all over the signal so it's impossible to
%filter it using bandpass filters

%__________________________________________________________________________

%% 2. Frequency thresholding in 2D
%% 2.1 loading image and viewing it
%clear variables that are not used further
clearvars
%_________________________________________

Image = load(fullfile(matlabroot,'examples','wavelet', 'jump.mat')); %load image
figure('Name', 'Original Image');
imshow(Image.jump, []); %view image
%% 2.2 Showing the Fourier spectrum of the signal in 2-D
Image.fourier = fft2 (Image.jump);
Image.fourier = fftshift(Image.fourier); % Center FFT

Image.magnitude = abs(Image.fourier); % Get the magnitude
Image.magnitude = log(Image.magnitude + 1); % Use log, for perceptual scaling, and +1 since log(0) is undefined
Image.magnitude = mat2gray(Image.magnitude); % Use mat2gray to scale the image between 0 and 1

figure('Name', '2-D signal spectrum');
imshow(Image.magnitude, []); % Display the spectrum of the signal in 2-D
%% 2.3 Setting an appropriate hard or soft threshold

Image.hardThreshold = zeros(986, 1577);%hard window initialization
Image.hardThreshold (250:750, 550:1050) = 1; %hard window bounds setup
%note that the smaller are bounds,  than softer image will be as filtered result

%Looking at the signal spectrum shown in 2.2 we see a "bright star" in the
%middle of the figure. These values seems to be the most valuable.
%We can make different soft borders (choose values up from the specified value)
%As an exaple we get rid of coeffs, that are lower 0.5:
Image.softThreshold = round(Image.magnitude);%use round to make a soft threshold window

imshow(Image.hardThreshold, []); % display hard ts
imshow(Image.softThreshold,[]); % display soft ts
%% 2.4 Showing the filtered Fourier spectrum of the signal
%let's show how threshold affects spectrum
Image.hardFilteredMagnitude = Image.magnitude .* Image.hardThreshold;
Image.softFilteredMagnitude = Image.magnitude .* Image.softThreshold;

figure('Name', 'Hard Filtered Fourier Spectrum');
imshow(Image.hardFilteredMagnitude, []);
figure('Name', 'Soft Filtered Fourier Spectrum');
imshow(Image.softFilteredMagnitude, []);
%% 2.5 Showing the reconstructed image and the residuals
%finally, apply our filter to fourier and take inverse fft to view result
Image.hardFiltered = Image.fourier .* Image.hardThreshold;
Image.hardFiltered = ifft2(ifftshift(Image.hardFiltered));% take inverse fourier and shift back

Image.softFiltered = Image.fourier .* Image.softThreshold;
Image.softFiltered = ifft2(ifftshift(Image.softFiltered));

figure('Name', 'Hard Filtered');
imshow(Image.hardFiltered, []);
figure('Name', 'Soft Filtered');
imshow(Image.softFiltered, []);
%as we can see, soft filtering that was used in 2.3 brings very blurry image (many coeffs are put to 0)
%to see image more detailed, we could change borders -> wider them. Speaking about detalization, some noise is normal.
%We can adjust noise removal by resizing threshold bounds.

%_____________________________________________________________________________

%% 3. Wavelet transformation
%clear variables that are not used further
clearvars
%_________________________________________
%load image again
Image = load(fullfile(matlabroot,'examples','wavelet', 'jump.mat')); %load image
figure('Name', 'Original Image');
imshow(Image.jump, []); %view image
Image = Image.jump; %make variable to work with

%using wavelet GUI we could try different wavelets (types and levels) and
%compare de-noising results

%at first I used 'haar' level-2 with automatically generated "scaled AWGN"
%threshold

%Haar wavelets are one of the most simple, so I used them first to see how
%they affect our image and to show later that they are quite rough
%comparing with bior wavelets.

%resulting de-noising progress was saved to 'waveletdenoise.m' file
denoisedImage = waveletdenoise(Image); %call function that makes de-noising
figure('Name', 'Wavelet haar level-2 filtered image');
imshow(denoisedImage, []);
%we get rid of some noise but it's still too sharp.
%trying to increase wavelet-level and different types will help to reach
%good results

%lets see how 'bior3.5' 5-level wavelet will work
waveletName = 'bior3.5';
waveletLevel = 5;
[decompositionVector, bookkepingMatrix] = wavedec2(Image, waveletLevel, waveletName); % returns the wavelet decomposition 
%of our 'noisedImage' at level 'waveletLevel' using the wavelet named in character vector
thresholdValues = wthrmngr('dw2ddenoLVL', 'penalhi', decompositionVector, bookkepingMatrix, 3); %returns threshold due to options
%more information at https://www.mathworks.com/help/wavelet/ref/wthrmngr.html
SORH = 's'; %Soft Or Hard thresholding
[XDEN,cfsDEN,dimCFS] = wdencmp('lvd', decompositionVector, bookkepingMatrix, waveletName, waveletLevel, thresholdValues, SORH); %denoising process function
figure('Name', 'Wavelet bior3.5 level-5 filtered image');
imshow(XDEN, []);
%we cound see that bior3.5 gives much more accurate view without noise but
%still sharp enough

%in GUI:
% *window most of the noise is spead across low levels of wavelet 
%decomposition, to reduce it bigger values have to be set in GUI
% *reconstruction of image was made automatically due to profile settings set
%to de-noising

%____________________________________________________________________________

%% 4. Singular Value Decomposition
%% 4.1 load image
%clear variables that are not used further
clearvars
%_________________________________________
Image = load(fullfile(matlabroot,'examples','wavelet', 'jump.mat')); %load image
Image.noised = im2double(Image.jump); %convert values of image to double to work with svd
%% 4.2 decomposition and plotting singular values
[SVD.U, SVD.S, SVD.V] = svd(Image.noised); %decomposing and image
figure('Name', 'Noised Image');
imshow(Image.noised,[]); %show noised image

SVD.singVal = diag(SVD.S);%set singular values variable
%show all singular values
figure('Name', 'Singular Values');
semilogy(SVD.singVal(1:length(SVD.singVal)),'-ro','LineWidth',1);
legend('singular values');
grid on;

%% 4.3 singular values contribution
%lets have a look at first 10 singular values and theit contribution to an
%image
SVD.variance = zeros(986,1577);
for i = 1 : 10
    SVD.variance(i,i) = SVD.singVal(i); %writes values from singVal to variance every step
    Image.partReconstructed(:,:,i) = SVD.U * SVD.variance * ((SVD.V)'); %reconstruction of image = U*S*V'
end

%lets make a figure with tabs to see singular values contribution to image
for i = 1 : 10
    currentTab = uitab('Title', ['SV 1:',num2str(i)]); %create tabs with different titles
    axes('Parent', currentTab); %add tabs to figure
    
    colormap(gray(256)); %change colormap to grayscale
    imagesc(Image.partReconstructed(:,:,i));
end
%we see how image changes every time with more singular values used for it
%reconstruction

%to make more accurate image, we need to take little more than 10 singular values
SVD.singularNum = 50; %we can play using different number of singular values
SVD.variance = SVD.S;
SVD.variance(SVD.singularNum:end, SVD.singularNum:end) = 0;
Image.reconstructed = SVD.U * SVD.variance * ((SVD.V)');
figure('Name', ['Image reconstruction using ',num2str(SVD.singularNum),' singular values'])
imshow(Image.reconstructed, []); %show results

%___________________________________________________________________________

%% 5. HCA on satellite images
%clear variables that are not used further
clearvars
%_________________________________________

% We use a hyperspectral data set with 8 detection windows of the same area. 
imageOriginal = im2double(imread('RomanColosseum_WV2mulitband.tif')); %load image
imageReshaped = reshape(imageOriginal, [419*658,8]); %reshapind 3-D to 2-D

Cluster.level = 18; %set the level of clustering here (amount of groups our pixels will be divided)
Cluster.indices = kmeans(imageReshaped, Cluster.level, 'MaxIter', 200); %using k-means to perform k-means clustering with levels, set by clusteringLevel variable
Cluster.indReshaped = reshape(Cluster.indices, [419,658]); %take index vector and reshape it to 419x658 (original dimension) 2D image

figure
colormap(gray(256)); %change colormap to grayscale
imagesc(Cluster.indReshaped); %show results

%the more levels we use, then higher is precision